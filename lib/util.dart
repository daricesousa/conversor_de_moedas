import 'package:flutter/material.dart';

Color colorMain = Colors.purple[700];

Widget textFiel(prefixo, textoLabel, controle, Function funcao) {
  return TextField(
    controller: controle,
    keyboardType: TextInputType.number,
    style: TextStyle(
      fontSize: 25,
      color: colorMain,
    ),
    decoration: InputDecoration(
      // filled: true,
      // fillColor: Color(0xFFF2F2F2),
      labelText: textoLabel,
      labelStyle: TextStyle(color: colorMain, fontSize: 25.0),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: colorMain),
      ),
      border: OutlineInputBorder(),
      prefixText: prefixo,
    ),
    onChanged: funcao,
  );
}

Widget textoCarregando(texto) {
  return Center(
    child: Text(
      texto,
      textAlign: TextAlign.center,
      style: TextStyle(color: colorMain, fontSize: 25),
    ),
  );
}
